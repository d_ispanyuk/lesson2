import React, { useState, useEffect } from "react";

export const MyContext = React.createContext({});

const Provider = ({ children, data }) => {
  const [searchValue, setSearchValue] = useState("");

  useEffect(() => {
    console.log(searchValue)
  }, [searchValue])

  return (
    <MyContext.Provider value={{ searchValue, setSearchValue, data }}>
      {children}
    </MyContext.Provider>
  );
};

export const MyContextProvider = Provider;

