import logo from "./logo.svg";
import "./App.css";
import { Test } from "./Test";
import { useCallback, useMemo, useRef, useState, useEffect, useContext } from "react";
import { MyContext, MyContextProvider } from "./context";

/*
onClick
onChange
onMouseDown (up)
onBlur
onFocus

*/

const Tree = () => {
  const state = useContext(MyContext);
  return <div>{state.searchValue}</div>
}

function App() {
  return (
    <>
    <MyContextProvider data={[]}>
      <Test />
    </MyContextProvider>
      <Tree />
      </>
  );
}

export default App;
