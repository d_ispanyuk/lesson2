import React, { useContext, useEffect, useState } from "react";
import { MyContext } from "./context";

export const Test = ({ name, onClick }) => {
    const { searchValue, setSearchValue } = useContext(MyContext)


  return (
    <input value={searchValue} onChange={(e) => {
        setSearchValue(e.target.value)
    }}/>
  );
};
